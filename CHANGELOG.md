# Versions:
## Version 1.2 (02/2019):
- A progress bar has been added !

## Version 1.1 (02/2019):
- You can scan a range of ports or only one

## Version 1.0 (02/2019):
- Function to check if the host is up
- Function to scan ports