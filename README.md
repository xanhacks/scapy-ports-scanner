# Scapy Ports Scanner

Ports scanner made with Scapy and Python3 !

## Installation:

- Python 3.X (https://www.python.org/)

```bash
$ pip install scapy
$ pip install tqdm
```
*"tqdm" is not required.*

## Usage:

```bash
$ sudo ./ports_scanner.py <HOST> <PORT>
$ sudo ./ports_scanner.py <HOST> <MIN_PORT> <MAX_PORT>
```

**In first, it will check if the host is up and after that, scan the specified port or ports between min and max.**

Very easy to use !