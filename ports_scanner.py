#!/usr/bin/env python3
import datetime
from scapy.all import *
from tqdm import tqdm


def main():
    file_name = os.path.basename(__file__)

    if os.geteuid() != 0:
        print("[ERROR] Please run this script as root !")
        sys.exit(1)

    if len(sys.argv) == 3:
        sys.argv.append(sys.argv[2])
    if len(sys.argv) != 4:
        print("[ERROR] Wrong usage !")
        print("Example: ./{0} <HOST> <PORT>".format(file_name))
        print("Example: ./{0} <HOST> <MIN_PORT> <MAX_PORT>".format(file_name))
        sys.exit(1)

    try:
        sys.argv[2] = int(sys.argv[2])
        sys.argv[3] = int(sys.argv[3])
    except ValueError:
        print("[ERROR] Wrong usage !")
        print("Example: ./{0} <HOST> <PORT>".format(file_name))
        print("Example: ./{0} <HOST> <MIN_PORT> <MAX_PORT>".format(file_name))
        print("Port must be integer between 1 and 65535")
        sys.exit(1)

    ping(sys.argv[1])

    scan(sys.argv[1], sys.argv[2], sys.argv[3])


def ping(host):
    timeout = 1
    print("[INFO] Checking {0} ...".format(host))
    conf.verb = 0

    try:
        packet = IP(dst=host)/ICMP()
        reply = sr1(packet, timeout=timeout)
        print("[{}] {} ({}) is up !".format(time.strftime("%H:%M:%S"), host, packet[IP].dst))
        print(" ")
    except socket.gaierror:
        print("[{}] {} is down !".format(time.strftime("%H:%M:%S"), host))
        print("[!] Exiting ...")
        sys.exit(1)


def scan(host, minport, maxport):
    timeout = 0.2
    src_port = RandShort()

    if minport < 1:
        minport = 1
    if maxport > 65535:
        maxport = 65535
    if minport == maxport:
        maxport += 1
    if minport > maxport:
        tmp = maxport
        maxport = minport
        minport = tmp

    print("[INFO] Scanning {} ...".format(host))

    open_ports = []

    try:
        for port in tqdm(range(minport, maxport), unit=" ports"):
            packet = IP(dst=host)/TCP(sport=src_port, dport=port, flags="S")
            reply = sr1(packet, timeout=timeout, verbose=0)

            try:
                if reply.haslayer(TCP) and reply.getlayer(TCP).flags == 0x12:
                    open_ports.append(port)
            except AttributeError:
                pass

        if len(open_ports) > 0:
            if len(open_ports) == 1:
                print("[{}] {} port open on {}:".format(time.strftime("%H:%M:%S"), len(open_ports), host))
            else:
                print("[{}] {} ports open on {}:".format(time.strftime("%H:%M:%S"), len(open_ports), host))
            for i in range(len(open_ports)):
                print("[{}] Port: {}".format(host, open_ports[i]))
        else:
            print("[{}] No port is open on {} !".format(time.strftime("%H:%M:%S"), len(open_ports), host))

    except NameError:
        t1 = datetime.now()
        print("[ERROR] Please install 'tqdm' module. (Not required)")
        for port in range(minport, maxport):
            packet = IP(dst=host)/TCP(sport=src_port, dport=port, flags="S")
            reply = sr1(packet, timeout=timeout, verbose=0)

            try:
                if reply.haslayer(TCP) and reply.getlayer(TCP).flags == 0x12:
                    print("[{}] Port {} open on {}".format(time.strftime("%H:%M:%S"), port, host))
            except AttributeError:
                if verbose:
                    print("[{}] Port {} close on {}".format(time.strftime("%H:%M:%S"), port, host))
                else:
                    pass

        t2 = datetime.now()
        duration = t2 - t1
        print("[INFO] Scan done in {} minute(s) and {} second(s) !"
              .format(int(duration.total_seconds() / 60), int(duration.total_seconds() % 60)))

    except KeyboardInterrupt:
        print("\n[!] Exiting ...")
        sys.exit(1)


if __name__ == "__main__":
    verbose = False

    main()